
import * as cakeTypes from './cakeTypes'

const initialState = {
    numberOfCakes: 10
}

const cakeReducer = (state = initialState, action) => {
    switch (action.type) {
        case cakeTypes.BUY_CAKE: 
            return {
                ...state,
                numberOfCakes: state.numberOfCakes - 1
            }
            
            break;
    
        default:
            return state
            break;
    }
}

export default cakeReducer