/* eslint-disable react/react-in-jsx-scope */
import React from 'react'
import { connect } from 'react-redux'
import { buyCake } from '../redux/cakes/cakeActions';

const CakeContainer = (props) => {
    console.log(props)
    return ( 
        <div>
            <h2>Number of cakes - {props.numberOfCakes}</h2>
            <button onClick={props.buyCake}>Buy cake</button>
        </div>
     );
}

const mapStateToProps = state => {
    console.log(state)
    return {
        numberOfCakes: state.numberOfCakes
    }
}


const mapDispatchToProps = dispatch => {
    console.log(dispatch(buyCake()))
    return {
        buyCake: () => {
            dispatch(buyCake())
        }
    }
}
 
export default connect(mapStateToProps, mapDispatchToProps)(CakeContainer);